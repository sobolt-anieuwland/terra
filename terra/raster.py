# Standard library imports
from pathlib import Path
from typing import Dict, Any, Union, Iterator, Tuple

# Third party imports
import rasterio as rio
import geopandas as gpd
from shapely.geometry import Polygon, Point


class Raster:
    VECTOR_FORMATS: Dict[str, str] = {
        "shp": "ESRI Shapefile",
        "gpkg": "GPKG"
    }

    __path: Path
    __handle: Any

    def __init__(self, filepath: str):
        self.__path = Path(filepath)
        if not self.__path.exists():
            raise ValueError(f"File {filepath} doesn't exist")

        self.__handle = rio.open(filepath)

    def bounds_to_file(self, out_file: Union[str, Path], driver="gpkg") -> bool:
        assert driver in self.VECTOR_FORMATS, "Unsupported vector format"
        driver = self.VECTOR_FORMATS[driver]

        crs, polygon = self.bounds
        gdf = gpd.GeoDataFrame()
        gdf["geometry"] = [polygon]
        gdf.crs = crs

        path = out_file if isinstance(out_file, str) else str(out_file)
        return gdf.to_file(path, driver=driver)

    def tile_masks_to_file(
            self, width_pixels: float, height_pixels: float,
            out_file: Union[str, Path], driver="gpkg"
    ) -> bool:
        assert driver in self.VECTOR_FORMATS, "Unsupported vector format"
        driver = self.VECTOR_FORMATS[driver]

        crs = self.crs
        gdf = gpd.GeoDataFrame()
        tiles = list(self.generate_tiles_by_pixel_size(width_pixels, height_pixels))
        gdf["geometry"] = tiles
        gdf.crs = crs

        path = out_file if isinstance(out_file, str) else str(out_file)
        return gdf.to_file(path, driver=driver)

    def generate_tiles_by_pixel_size(
        self, width_pixels: float, height_pixels: float
    ) -> Iterator[Polygon]:
        _, bounds = self.bounds
        start_x, start_y = bounds.exterior.coords[0]
        end_x, end_y = bounds.exterior.coords[2]

        # Ensure smaller values in start variables, so we can sum positively
        if start_x > end_x:
            end_x, start_x = start_x, end_x
        if start_y > end_y:
            end_y, start_y = start_y, end_y

        # Convert the desired tile size from pixels to the CRS's unit
        res_x, res_y = self.resolution
        width_crs = abs(width_pixels * res_x)
        height_crs = abs(height_pixels * res_y)

        # Create a generator yielding tiles as Polygons
        tile_top = start_y
        while tile_top < end_y:
            tile_left = start_x
            while tile_left < end_x:
                p1 = Point(tile_left, tile_top)
                p2 = Point(tile_left + width_crs, tile_top)
                p3 = Point(tile_left + width_crs, tile_top + height_crs)
                p4 = Point(tile_left, tile_top + height_crs)

                tile_left += width_crs
                pol = Polygon([p1, p2, p3, p4])
                yield pol
            tile_top += height_crs

        # Explicitly end the generator by returning None
        return None

    @property
    def bounds(self) -> (Any, Polygon):
        """ Get raster's bounding box with its accompanying CRS.

            # Returns
            (Any, Polygon): A tuple with at index 1 the CRS of the polygon and
                at index 2 a shapely polygon of the bounding box.
        """
        bounds = self.__handle.bounds

        p1 = Point(bounds[0], bounds[3])
        p2 = Point(bounds[2], bounds[3])
        p3 = Point(bounds[2], bounds[1])
        p4 = Point(bounds[0], bounds[1])

        box = Polygon([p1, p2, p3, p4, p1])
        return (self.__handle.crs, box)

    @property
    def resolution(self) -> Tuple[float, float]:
        res = self.__handle.transform
        return (res[0], res[4])

    @property
    def crs(self) -> Any:
        return self.__handle.crs
