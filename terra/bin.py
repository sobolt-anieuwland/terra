from argparse import ArgumentParser
from terra import Raster


def execute():
    args = parse_arguments()

    # Map commands to functions to handle them
    cmd_mappings = {
        "raster": handle_raster_commands
    }

    # Check whether command is mapping before executing
    if args.cmd not in cmd_mappings:
        raise ValueError("No function for command")

    # Execute selected command
    function = cmd_mappings[args.cmd]
    function(args)


def parse_arguments():
    parser = ArgumentParser()
    commands_parser = parser.add_subparsers()
    commands_parser.required = True
    commands_parser.dest = "cmd"

    raster_parser = commands_parser.add_parser("raster")
    raster_parser_subcmd = raster_parser.add_subparsers(required=True)
    raster_parser_subcmd.dest = "subcmd"

    raster_bounds_parser = raster_parser_subcmd.add_parser("get-bounds")
    raster_bounds_parser.add_argument(
        "-of", "--out-format", type=str, choices=Raster.VECTOR_FORMATS.keys(),
        default="gpkg"
    )
    # raster_bounds_parser.add_argument("-op", "--out-projection", type=str)  # TODO
    raster_bounds_parser.add_argument("in_file", metavar="IN_FILE", type=str)
    raster_bounds_parser.add_argument("out_file", metavar="OUT_FILE", type=str)

    return parser.parse_args()


################################################################################
#### Raster commands
################################################################################
def handle_raster_commands(args):
    cmd_mappings = {
        "get-bounds": raster_get_bounds
    }

    # Check whether command is mapping before executing
    if args.subcmd not in cmd_mappings:
        raise ValueError("No function for command")

    function = cmd_mappings[args.subcmd]
    function(args)


def raster_get_bounds(args):
    raster = Raster(args.in_file)
    raster.bounds_to_file(args.out_file, args.out_format)


################################################################################
if __name__ == "__main__":
    execute()
